# Docs

[Checkout the Wiki!](https://gitlab.cern.ch/cms-cactus/docs/-/wikis/)

## Editing the wiki

You can either edit in the GitLab interface or checkout the associated repo with:

```bash
git clone https://:@gitlab.cern.ch:8443/cms-cactus/docs.wiki.git
```

and edit locally like a normal git repo.
